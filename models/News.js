'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NewsSchema = new Schema({
  publication_date: String,
  url: String,
  source: String,
  nationality: String,
  language: String,
  title: String,
  title_en: String,
  description: String,
  summarization: String,
  countries: [String],
  cities: [{
    city_names: String,
    population: Number,
    location: {
      type: { type: String},
      coordinates: [],
    }
  }]
});

NewsSchema.indexes({ keywords: "text" }, { default_language: "en" });
NewsSchema.index({ "cities.location" : "2dsphere" });

const News = mongoose.model('News', NewsSchema);
module.exports = News;
