const express = require('express')
const uuidv1 = require('uuid/v1');
const News = require('../models/News');
const router = express.Router();


router.get("/by_nationality", (req, res, next) => {
  News.aggregate([{
      $group: {
        _id: "$nationality",
        news_count: {
          $sum: 1
        },
        news_grouped: {
          $push: {
            news_id: "$_id",
            publication_date: "$publication_date",
            url: "$url",
            source: "$source",
            language: "$language",
            summary: "$summarization",
            countries: "$countries",
            cities: "$cities.city_names"
          }
        }
      }
    },
    {
      $sort: {
        "news_grouped.publication_date": -1,
        news_count: -1
      }
    },
    {
      $project: {
        _id: uuidv1(),
        nationality: "$_id",
        news_count: "$news_count",
        news_grouped: "$news_grouped"
      }
    }
  ]).then((items) => {
    res.send(items);
  }).catch((err) => {
    res.status(500);
    res.send(err);
  });
});


router.get("/closest", (req, res, next) => {
  const lat = parseInt(req.query.lat);
  const lon = parseInt(req.query.lon);
  const language = req.query.language || 'en';
  const distance = 300; // default 100 km

  News.aggregate(
    [{
        $geoNear: {
          near: {
            type: "Point",
            coordinates: [lon, lat]
          },
          maxDistance: distance * 1000,
          distanceField: 'distance',
          spherical: true
        }
      },
      {
        $match: {
          language: language
        }
      },
      {
        $project: {
          _id: 0,
          news_id: "$_id",
          publication_date: "$publication_date",
          url: "$url",
          source: "$source",
          language: "$language",
          nationality: "$nationality",
          summary: "$summarization",
          description: "$description",
          countries: "$countries",
          cities: "$cities.city_names"
        }
      }
    ]
  ).then((items) => {
    console.log("Number of results", items.length)
    res.send(items);
  }).catch((error) => {
    res.status(500);
    res.send(error);
  });
});


router.post("/search", (req, res, next) => {
  let input, words;
  input = req.query.querySearch || '';
  input = input.toLowerCase();
  words = input.split(" ");

  News.aggregate([{
      $match: {
        keywords: {
          $in: words
        }
      }
    },
    {
      $sort: {
        publication_date: -1
      }
    },
    {
      $project: {
        _id: 0,
        news_id: "$_id",
        publication_date: "$publication_date",
        url: "$url",
        source: "$source",
        language: "$language",
        nationality: "$nationality",
        summary: "$summarization",
        description: "$description",
        countries: "$countries",
        cities: "$cities.city_names"
      }
    }
  ]).then((items) => {
    res.send(items);
  }).catch((err) => {
    res.status(500);
    res.send(err);
  });
});


router.get("/", (req, res, next) => {
  const nDays = req.query.nDays || 0;
  const now = new Date();
  const before = new Date(now - nDays * (24 * 60 * 60 * 1000));

  News.aggregate([{
      $match: {
        publication_date: {
          $gte: before,
          $lte: now
        }
      }
    },
    {
      $sort: {
        publication_date: -1
      }
    },
    {
      $project: {
        _id: 0,
        news_id: "$_id",
        url: "$url",
        source: "$source",
        language: "$language",
        nationality: "$nationality",
        summary: "$summarization",
        description: "$description",
        countries: "$countries",
        publication_date: "$publication_date",
        "cities": "$cities.city_names"
      }
    }
  ]).then((items) => {
    res.send(items);
  }).catch((err) => {
    res.status(500);
    res.send(err);
  });
});

module.exports = router;