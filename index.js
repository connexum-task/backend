'use strict';
const express = require("express");
const mongoose = require("mongoose");
const morgan = require('morgan');
const newsRoutes = require('./routes/news');
const app = express();


mongoose.connect('mongodb://127.0.0.1/connexum', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(morgan('tiny'))
app.use('/news', newsRoutes);

app.listen(3000, () => {
 console.log("Server running on port 3000");
});
